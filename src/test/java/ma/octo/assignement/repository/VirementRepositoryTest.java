package ma.octo.assignement.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ma.octo.assignement.domain.Virement;

import static org.assertj.core.api.Assertions.assertThat;

import javax.transaction.Transactional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class VirementRepositoryTest {

  @Autowired
  private VirementRepository virementRepository;

  @Test
  public void findOne() {
	  assertThat(virementRepository.findById(5l)).isNotEmpty();
  }

  @Test
  public void findAll() {
	  assertThat(virementRepository.findAll()).isNotNull();
  }

  @Test
  public void save() {
	  
	  Virement v = virementRepository.findById(5l).get();
	  
	  v.setMotifVirement("Test virement repository");
	  
	  virementRepository.save(v);
	  
	  
  }

  @Test
  public void delete() {
	  
	  Virement v = virementRepository.findById(5l).get();
	  
	  virementRepository.delete(v);
	  
  }
}