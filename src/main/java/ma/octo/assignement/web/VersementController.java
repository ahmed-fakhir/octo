package ma.octo.assignement.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.iservice.IAuditVersementService;
import ma.octo.assignement.iservice.ICompteService;
import ma.octo.assignement.iservice.IVersementService;

@RestController
public class VersementController {
	
	@Autowired
	private IVersementService versementService;
	
	@Autowired
	private ICompteService compteService;
	
	@Autowired
	private IAuditVersementService auditVersementService;
	
	@GetMapping("lister_versements")
    public List<Versement> loadAll() {
        return versementService.findAll();
    }
	
	
	@PostMapping("/executerVersement")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody VersementDto versementDto) throws CompteNonExistantException, TransactionException {
		
		Versement v = new Versement();
		
		v.setMontantVersement(versementDto.getMontantVersement());
		v.setMotifVersement(versementDto.getMotif());
		v.setNom_prenom_emetteur(versementDto.getNom_prenom_emetteur());
		v.setCompteBeneficiaire(compteService.findByNrCompte(versementDto.getNrCompteBeneficiaire()));
		
		versementService.executerVersement(v);
		
		auditVersementService.auditVersement("Versement effectué par la personne " + versementDto.getNom_prenom_emetteur() + " vers " + 
					versementDto.getNrCompteBeneficiaire() + " d'un montant de " + versementDto.getMontantVersement().toString());
		
	}
}