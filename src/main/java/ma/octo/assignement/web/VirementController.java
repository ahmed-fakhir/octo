package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.iservice.IAuditVirementService;
import ma.octo.assignement.iservice.ICompteService;
import ma.octo.assignement.iservice.IUtilisateurService;
import ma.octo.assignement.iservice.IVirementService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController(value = "/virements")
class VirementController {

    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(VirementController.class);

    @Autowired
    private ICompteService compteService;
    @Autowired
    private IVirementService virementService;
    @Autowired
    private IAuditVirementService auditVirementService;
    @Autowired
    private IUtilisateurService utilisateurService;
    
    

    @GetMapping("lister_virements")
    public List<Virement> loadAll() {
        return virementService.loadAll();
    }

    @GetMapping("lister_comptes")
    List<Compte> loadAllCompte() {
        return compteService.findAll();
    }

    @GetMapping("lister_utilisateurs")
    public List<Utilisateur> loadAllUtilisateur() {
        return utilisateurService.loadAllUtilisateur();
    }

    @PostMapping("/executerVirements")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody VirementDto virementDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        

        Virement virement = new Virement();
        
        virement.setDateExecution(virementDto.getDate());
        virement.setCompteBeneficiaire(compteService.findByNrCompte(virementDto.getNrCompteEmetteur()));
        virement.setCompteEmetteur(compteService.findByNrCompte(virementDto.getNrCompteBeneficiaire()));
        virement.setMontantVirement(virementDto.getMontantVirement());
        virement.setMotifVirement(virementDto.getMotif());

        virementService.executeVirement(virement);

        auditVirementService.auditVirement("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
                        .getNrCompteBeneficiaire() + " d'un montant de " + virementDto.getMontantVirement()
                        .toString());
    }

}
