package ma.octo.assignement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.iservice.ICompteService;
import ma.octo.assignement.repository.CompteRepository;

@Service
public class CompteService implements ICompteService {
	
	
	 @Autowired
	 private CompteRepository rep;
	 
	 
	 public Compte findByNrCompte(String nrCompte) throws CompteNonExistantException {
		 Compte c1 = rep.findByNrCompte(nrCompte);
		 
		 if (c1 == null) {
	            System.out.println("Compte Non existant");
	            throw new CompteNonExistantException("Compte Non existant");
	     }
		 
		 return c1;
	 }
	 
	 
	 
	 public void save(Compte c) {
		 rep.save(c);
	 }
	 
	 
	 public List<Compte> findAll() {
		 List<Compte> all = rep.findAll();

	     if (CollectionUtils.isEmpty(all)) 
	    	 return null;
	     
	     return all;
	        
	 }
	 
	 
}