package ma.octo.assignement.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.AuditVersement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.iservice.IAuditVersementService;
import ma.octo.assignement.repository.AuditVersementRepository;

@Service
public class AuditVersementService implements IAuditVersementService {
	
	@Autowired 
	private AuditVersementRepository rep;
	
	Logger LOGGER = LoggerFactory.getLogger(AuditVersementService.class);

	
	public void auditVersement(String m) {

        LOGGER.info("Audit de l'événement {}", EventType.VERSEMENT);
        
        AuditVersement a = new AuditVersement();
        
        a.setMessage(m);
        a.setEventType(EventType.VERSEMENT);
		rep.save(a);
    }
	
}