package ma.octo.assignement.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.iservice.ICompteService;
import ma.octo.assignement.iservice.IVersementService;
import ma.octo.assignement.repository.VersementRepository;


@Service
public class VersementService implements IVersementService {
	
	@Autowired 
	private VersementRepository rep;
	
	@Autowired
	private ICompteService compteService;
	
	private static final int MONTANT_MAXIMAL = 10000;
	
	private Logger LOGGER = LoggerFactory.getLogger(VersementService.class);
	
	
	public void executerVersement(Versement v) throws TransactionException {

        if (v.getMontantVersement().equals(null)) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (v.getMontantVersement().intValue() == 0) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (v.getMontantVersement().intValue() < 10) {
            System.out.println("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (v.getMontantVersement().intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }

        if (v.getMotifVersement().length() <= 0) {
            System.out.println("Motif vide");
            throw new TransactionException("Motif vide");
        }

        v.getCompteBeneficiaire().setSolde(v.getCompteBeneficiaire().getSolde().add(v.getMontantVersement()));
        compteService.save(v.getCompteBeneficiaire());
        
        
        v.setDateExecution(new Date());

        rep.save(v);
		
		
	}
	
	
	
	
	
	public List<Versement> findAll() {
		List<Versement> l =  rep.findAll();
		
		if (l.isEmpty())
			return null;
		
		return l;
	}
	
	
}