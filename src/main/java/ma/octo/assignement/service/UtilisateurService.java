package ma.octo.assignement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.iservice.IUtilisateurService;
import ma.octo.assignement.repository.UtilisateurRepository;

@Service
public class UtilisateurService implements IUtilisateurService {
	
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
	public List<Utilisateur> loadAllUtilisateur() {
        List<Utilisateur> all = utilisateurRepository.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
    }
	
	
	
	public void save(Utilisateur u) {
		utilisateurRepository.save(u);
	}
	
}