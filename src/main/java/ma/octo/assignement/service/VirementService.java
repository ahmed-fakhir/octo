package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.iservice.ICompteService;
import ma.octo.assignement.iservice.IVirementService;
import ma.octo.assignement.repository.VirementRepository;

@Service
public class VirementService implements IVirementService {
	
	@Autowired
	private VirementRepository virementRepository;
	
	@Autowired
	private ICompteService compteService;
	
	public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(VirementService.class);
	
	public List<Virement> loadAll() {
		List<Virement> all = virementRepository.findAll();

        if (CollectionUtils.isEmpty(all)) 
            return null;
        
        return all;
	}
	
	public void save(Virement v) {
		virementRepository.save(v);
	}
	
	
	public void executeVirement(Virement v) throws SoldeDisponibleInsuffisantException, TransactionException {
		
		if (v.getMontantVirement().equals(null)) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (v.getMontantVirement().intValue() == 0) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (v.getMontantVirement().intValue() < 10) {
            System.out.println("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (v.getMontantVirement().intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }
		
		if (v.getMotifVirement() == null) {
            System.out.println("Motif null");
            throw new TransactionException("Motif null");
        }

        if (v.getMotifVirement().length() <= 0) {
            System.out.println("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (v.getCompteEmetteur().getSolde().intValue() - v.getMontantVirement().intValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
        }
        
        

        v.getCompteEmetteur().setSolde(v.getCompteEmetteur().getSolde().subtract(v.getMontantVirement()));
        compteService.save(v.getCompteEmetteur());

        v.getCompteBeneficiaire().setSolde(new BigDecimal(v.getCompteBeneficiaire().getSolde().intValue() + v.getMontantVirement().intValue()));
        compteService.save(v.getCompteEmetteur());

        virementRepository.save(v);
		
		
		
	}
	
}