## Description :

Un virement est un transfert d'argent d'un compte emetteur vers un compte bénéficiaire ...

**Besoin métier :** 

Ajouter un nouveau usecase versement. Le Versement est un dépôt d'agent sur un compte donné . 

Le versement est une opération trés utile lors des transfert de cash .
 
Imaginez que vous allez à une agence avec un montant de 1000DH et que vous transferez ça en spécifiant le RIB souhaité .
 
L'identifiant fonctionnel d'un compte dans ce cas préçis est le RIB .  







# Travail Fait

## Implémentation du UseCase (Versement) demandé

## Bug corrigés
* Vérification que "motif" n'est pas null ou vide.
* L'élément "motif" n'est pas enregistré dans la BD.
* SoldeDisponibleInsuffisantException oublié.
* La même instruction executé deux fois dans le code du VirementController.

## Lisibilité du code, bonnes pratiques et abstraction
* Versement modèle (variables nommés virement au lieu de versement).
* Méthode non utilisé de VirementControlller.
* AuditVirementService correction nom.
* Separation AuditVirementService et AuditVersementService.
* Les variables ne sont pas bien nommés.
* Ajout de la couche service.
* Ajout de l'abstraction pendant l'utilisation de la couche service.

## Réalisation de quelques testes unitaires
